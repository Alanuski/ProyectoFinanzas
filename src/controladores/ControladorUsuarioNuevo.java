/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.alexco.db.exceptions.DBDriverNotFound;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.UsuarioDAO;
import vistas.JFrameAgregarCuenta;
import vistas.JFrameUsuarioNuevo;

/**
 *
 * @author root
 */
public class ControladorUsuarioNuevo implements ActionListener {

    JFrameUsuarioNuevo vistaUsuarioNuevo = new JFrameUsuarioNuevo();
    UsuarioDAO modeloUsuario = new UsuarioDAO();
    public static int usuarioNuevo_id = 0;

    public ControladorUsuarioNuevo() {

    }

    public ControladorUsuarioNuevo(JFrameUsuarioNuevo controladorUsuarioNuevo) {
        this.vistaUsuarioNuevo = controladorUsuarioNuevo;
        this.vistaUsuarioNuevo.jButtonContinuar.addActionListener(this);

    }

    public void InicializarUsuario() {
        JFrameUsuarioNuevo ventanaUsuarioNuevo = new JFrameUsuarioNuevo();
        ventanaUsuarioNuevo.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        char[] contrasenya = vistaUsuarioNuevo.jPasswordFieldContrasenya.getPassword();
        char[] repitaContrasenya = vistaUsuarioNuevo.jPasswordFieldRepitaContrasenya.getPassword();
        System.out.println("funciona");
        System.out.println("1: " + Arrays.toString(contrasenya));
        System.out.println("2: " + Arrays.toString(repitaContrasenya));
        if (!Arrays.toString(contrasenya).equals(Arrays.toString(repitaContrasenya))) {
            JOptionPane.showMessageDialog(vistaUsuarioNuevo, "Las contraseñas no coinciden");

        } else {
            String nombre = vistaUsuarioNuevo.jTextFieldNombre.getText();
            MySQLBD mysql = new MySQLBD().conectar();
            ResultSet comprobacionNombre = mysql.consultar("SELECT NombreUsuario FROM usuarios WHERE NombreUsuario='" + nombre + "';");
            try {
                boolean resul = comprobacionNombre.next();
                if (!resul) {
                    String apellido = vistaUsuarioNuevo.jTextFieldApellido.getText();
                    String DNI = vistaUsuarioNuevo.jTextFieldDNI.getText();
                    String email = vistaUsuarioNuevo.jTextFieldEmail.getText();
                    modeloUsuario.setNombreUsuario(nombre);
                    modeloUsuario.setApellido(apellido);
                    modeloUsuario.setDNI(DNI);
                    modeloUsuario.setEmail(email);
                    modeloUsuario.setPassword(String.valueOf(contrasenya));
                    modeloUsuario.setNivelUsuario_id(1);
                    try {
                        modeloUsuario.save();
                        getUsuarioNuevoID();
                        System.out.println("ID new user: " + this.usuarioNuevo_id);
                        JOptionPane.showMessageDialog(vistaUsuarioNuevo, "Se ha creado su usuario exitosamente");

                    } catch (DBDriverNotFound ex) {
                        Logger.getLogger(ControladorUsuarioNuevo.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        Logger.getLogger(ControladorUsuarioNuevo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    vistaUsuarioNuevo.dispose();
                    JFrameAgregarCuenta agregar = new JFrameAgregarCuenta();
                    agregar.setLocationRelativeTo(null);
                    agregar.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(vistaUsuarioNuevo, "AA Usuario existente, por favor elija otro nombre de usuario");
                    vistaUsuarioNuevo.dispose();
                    JFrameUsuarioNuevo ventanaUsuarioNuevo = new JFrameUsuarioNuevo();
                    ventanaUsuarioNuevo.setLocationRelativeTo(null);
                    ventanaUsuarioNuevo.setVisible(true);
                }
            } catch (SQLException ex) {
                Logger.getLogger(ControladorUsuarioNuevo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int getUsuarioNuevoID() throws SQLException {
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet resultadoID = mysql.consultar("SELECT usuario_id from usuarios WHERE nombreUsuario = " + "'"
                + modeloUsuario.getNombreUsuario() + "'" + ";");

        if (resultadoID != null) {
            try {
                resultadoID.next();
                System.out.println(resultadoID.getInt("usuario_id"));
            } catch (SQLException ex) {
                Logger.getLogger(ControladorIniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.usuarioNuevo_id = resultadoID.getInt("usuario_id");
        return usuarioNuevo_id;
    }

}
