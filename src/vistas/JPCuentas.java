/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controladores.ControladorIniciarSesion;
import controladores.MySQLBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author root
 */
public class JPCuentas extends javax.swing.JPanel {

    /**
     * Creates new form JPCuentas
     */
    public JPCuentas() {
        initComponents();
    }
    public static void jListCuentasBancarias(){
        MySQLBD mysql = new MySQLBD().conectar();
        ResultSet contador = mysql.consultar("SELECT NumeroIBAN FROM Cuentas INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="+ControladorIniciarSesion.ID+" && usuarios_cuentas.cuenta_id=cuentas.cuenta_id");
        ResultSet resultados = mysql.consultar("SELECT NumeroIBAN FROM Cuentas INNER JOIN usuarios_cuentas ON usuarios_cuentas.usuario_id="+ControladorIniciarSesion.ID+" && usuarios_cuentas.cuenta_id=cuentas.cuenta_id");
        String[]cuentasString ;
        int contador2=0;
        
        if(contador != null) {
            try {
                while (contador.next()) {
                    contador2++;
                }
            } catch (SQLException ex) {
                Logger.getLogger(JFrameAgregarPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        cuentasString= new String[contador2];
        int fila=0;
        if(resultados != null) {
            try {
                while (resultados.next()) {
                   cuentasString[fila]=resultados.getString("NumeroIBAN");
                   fila++;
                }
            } catch (SQLException ex) {
                Logger.getLogger(JFrameAgregarPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        jListCuentasBancarias.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = cuentasString;
            @Override
            public int getSize() { return strings.length; }
            @Override
            public String getElementAt(int i) { return strings[i]; }
        });

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonAñadirCuenta = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListCuentasBancarias = new javax.swing.JList<>();
        jLabel6 = new javax.swing.JLabel();

        setLayout(null);

        jButtonAñadirCuenta.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        jButtonAñadirCuenta.setText("Añadir Cuenta");
        jButtonAñadirCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAñadirCuentaActionPerformed(evt);
            }
        });
        add(jButtonAñadirCuenta);
        jButtonAñadirCuenta.setBounds(484, 18, 145, 55);

        jButton2.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        jButton2.setText("Editar");
        add(jButton2);
        jButton2.setBounds(484, 91, 145, 54);

        jButton3.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        jButton3.setText("Eliminar");
        add(jButton3);
        jButton3.setBounds(490, 330, 145, 51);

        jListCuentasBancarias();
        jListCuentasBancarias.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        jScrollPane1.setViewportView(jListCuentasBancarias);

        add(jScrollPane1);
        jScrollPane1.setBounds(30, 18, 436, 370);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/FondoSuave - copia.png"))); // NOI18N
        add(jLabel6);
        jLabel6.setBounds(-190, -100, 850, 530);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAñadirCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAñadirCuentaActionPerformed
       JFrameAgregarCuenta c=new JFrameAgregarCuenta();
        c.setLocationRelativeTo(null);
        c.setVisible(true);
    }//GEN-LAST:event_jButtonAñadirCuentaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButtonAñadirCuenta;
    private javax.swing.JLabel jLabel6;
    public static javax.swing.JList<String> jListCuentasBancarias;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables


}
