package modelos;

import java.sql.Date;
import com.alexco.db.dao.DAOField;
import java.sql.Date;
import com.alexco.db.dao.DAOBase;

public class CreditoDAO extends DAOBase {

    private String descripcion;
    private int credito_id;

    public int getCredito_id() {
        return credito_id;
    }

    public void setCredito_id(int credito_id) {
        this.credito_id = credito_id;
    }
    private int cuenta_id;
    private int tipoPeriodo_id;
    private String fechaCreacion;
    private String fechaVencimiento;
    private double tipoTAE;
    private double limite;
    private double saldo;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCuenta_id() {
        return cuenta_id;
    }

    public void setCuenta_id(int cuenta_id) {
        this.cuenta_id = cuenta_id;
    }

    public int getTipoPeriodo_id() {
        return tipoPeriodo_id;
    }

    public void setTipoPeriodo_id(int tipoPeriodo_id) {
        this.tipoPeriodo_id = tipoPeriodo_id;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public double getTipoTAE() {
        return tipoTAE;
    }

    public void setTipoTAE(double tipoTAE) {
        this.tipoTAE = tipoTAE;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public CreditoDAO() {
        super("Creditos");
        //addFieldDefinition(new DAOField("Credito_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
        addFieldDefinition(new DAOField("Cuenta_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("TipoPeriodo_id", java.sql.Types.INTEGER, false));
        addFieldDefinition(new DAOField("FechaCreacion", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("FechaVencimiento", java.sql.Types.DATE));
        addFieldDefinition(new DAOField("TipoTAE", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("Limite", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("Saldo", java.sql.Types.DOUBLE));
    }
}
