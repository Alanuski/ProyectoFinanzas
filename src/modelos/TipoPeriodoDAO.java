package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;

public class TipoPeriodoDAO extends DAOBase {

    private String descripcion;
    private int tipoPeriodo_id;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTipoPeriodo_id() {
        return tipoPeriodo_id;
    }

    public void setTipoPeriodo_id(int tipoPeriodo_id) {
        this.tipoPeriodo_id = tipoPeriodo_id;
    }

    public TipoPeriodoDAO() {
        super("tiposperiodos");
        addFieldDefinition(new DAOField("TipoPeriodo_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("Descripcion", java.sql.Types.VARCHAR, 200));
    }
}
