package modelos;

import com.alexco.db.dao.DAOBase;
import com.alexco.db.dao.DAOField;
import java.sql.Date;

public class NivelUsuarioDAO extends DAOBase {

    private double nivelUsuario;
    private int NivelUsuario_id;

    public int getNivelUsuario_id() {
        return NivelUsuario_id;
    }

    public void setNivelUsuario_id(int NivelUsuario_id) {
        this.NivelUsuario_id = NivelUsuario_id;
    }
    private String nombreNivelUsuario;

    public double getNivelUsuario() {
        return nivelUsuario;
    }

    public void setNivelUsuario(double nivelUsuario) {
        this.nivelUsuario = nivelUsuario;
    }

    public String getNombreNivelUsuario() {
        return nombreNivelUsuario;
    }

    public void setNombreNivelUsuario(String nombreNivelUsuario) {
        this.nombreNivelUsuario = nombreNivelUsuario;
    }

    public NivelUsuarioDAO() {
        super("NivelUsuarios");
        addFieldDefinition(new DAOField("NivelUsuario_id", java.sql.Types.INTEGER, true));
        addFieldDefinition(new DAOField("NivelUsuario", java.sql.Types.DOUBLE));
        addFieldDefinition(new DAOField("NombreNivelUsuario", java.sql.Types.VARCHAR, 60));

    }

}
